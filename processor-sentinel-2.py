#!/usr/bin/env python3

import re
import geojson
from shapely import wkt
import argparse
import datetime
import glob
import os
import subprocess
from zipfile import ZipFile
import multiprocessing
from functools import partial
import logging
import shutil
import json
import sys
from sentinelsat import SentinelAPI, geojson_to_wkt
import rasterio
from pyproj import Proj
import affine
import numpy
from urllib import request as rq
import requests
import xmltodict
import time
import tempfile


def convert_coords(coords_list, crs):
    src_proj = Proj(init='EPSG:4326')
    trg_proj = Proj(init=crs)
    xy_list = [trg_proj(c[0], c[1]) for c in coords_list[0]]
    x_array = numpy.array([c[0] for c in xy_list])
    y_array = numpy.array([c[1] for c in xy_list])
    return x_array, y_array


def poll_marine_traffic():
    api_key = os.environ['MARINETRAFFIC_API_KEY']
    mt_url = "https://services.marinetraffic.com/api/exportvessels/v:8/{}/protocol:jsono".format(api_key) 
    response = requests.get(mt_url)
    content = json.loads(response.text)[0]
    return content


def get_ais_position():
    """
    Poll marine traffic for ships position, tailored to single boat (KPH atm)
    returns lon, lat, heading, course, time
    """
    try:
        mt_dict = poll_marine_traffic()
    except:
        time.sleep(140) # sleep if need to repeat request to Marine Traffic
        mt_dict = poll_marine_traffic()

    res_dict= dict(lon=float(mt_dict['LON']),
                  lat=float(mt_dict['LAT']),
                  speed=float(mt_dict['SPEED'])/10.,
                  heading=int(mt_dict['HEADING']),
                  course=int(mt_dict['COURSE']),
                  timestamp=datetime.datetime.strptime(mt_dict['TIMESTAMP'], "%Y-%m-%dT%H:%M:%S")
                  )
    return res_dict

def buffer_over_boat(lon, lat, buffer_radius=100000.):
    """
    Create buffer over position
    """
    from shapely.geometry import Point
    import pyproj
    from shapely.ops import transform
    from functools import partial

    pr = pyproj.Proj(init="{}".format(request["crs"].lower()))
    project_func = partial(pyproj.transform,
                           pr,
                           pyproj.Proj(init="epsg:4326"))
    x, y = pr(lon, lat)
    pbuffer_xy = Point(x, y).buffer(buffer_radius, resolution=4, cap_style=3)
    pbuffer_geodetic = transform(project_func, pbuffer_xy)
    return pbuffer_geodetic

def create_empty_dst(fpath, coords_list, res, crs, dtype):
    a_pixel_width = res
    b_rotation = 0
    d_column_rotation = 0
    e_pixel_height = res

    crs = str(crs).upper()

    (x_array, y_array) = convert_coords(coords_list, crs)
    c_x_ul = x_array.min()
    f_y_ul = y_array.max()
    height = (y_array.max() - y_array.min()) / res
    width = (x_array.max() - x_array.min()) / res

    aff = affine.Affine(a_pixel_width,
                 b_rotation,
                 c_x_ul,
                 d_column_rotation,
                 -1 * e_pixel_height,
                 f_y_ul)

    dst = rasterio.open(fpath,
                        'w',
                        driver='GTiff',
                        height=height,
                        width=width,
                        count=1,
                        dtype=dtype,
                        crs=crs,
                        transform=aff,
                        nodata=0,
                        compress="LZW",
                        predict=2
                        )
    return dst

def percentile_peak(ifile):
    with rasterio.open(ifile, 'r') as dst:
        ubound = numpy.percentile(dst.read(1), 95)
    return ubound

def process_sentinel_scene(product, data_dir, output_filepath):
    """
    Extract the contents of the obtained file
    and put it in the right place
    """
    with ZipFile(os.path.join(data_dir, ".".join([product['identifier'], "zip"]))) as zf:
        zf.extractall(data_dir)
        input_paths = glob.glob(os.path.join(data_dir,
                                             ".".join([product['identifier'],
                                             "SAFE/GRANULE/L1C*/IMG_DATA/*_B*.jp2"])))
        for i, ifile in enumerate(input_paths):
            if re.search(r".*_B02.jp2", ifile):
                with tempfile.NamedTemporaryFile() as temp:
                    subprocess.call([
                        "gdal_translate",
                        "-scale",
                        "-ot", "Byte",
                        ifile,
                        temp.name])

                    subprocess.call([
                        "gdalwarp",
                        "-t_srs",
                        request["crs"],
                        "-srcnodata",
                        "0",
                        "-dstnodata",
                        "0",
                        "-r",
                        "bilinear",
                        temp.name,
                        output_filepath
                    ])


def read_credentials(filepath):
    with open(filepath, 'r') as credentials:
        user = credentials.readline().rstrip('\n')
        password = credentials.readline().rstrip('\n')
        return user, password

def download_sentinel_product(
                        product_key,
                        products,
                        api,
                        request,
                        output_path
                    ):

    print("Obtaining item {}".format(product_key))
    api.download(product_key, directory_path=output_path)


def remove_dir(path):
    for root, dirs, files in os.walk(path):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
        os.rmdir(root)


def main():

    p = argparse.ArgumentParser()
    p.add_argument("--log-file", default=None)
    p.add_argument("--output-file", default=None)
    p.add_argument("--request-file", default=None)
    p.add_argument("--download-dir", default=None)
    p.add_argument("--cleanup-dir", default=True, action="store_true")
    p.add_argument("-k", "--keep-temporary", action="store_true")
    p.add_argument("--download-only", action="store_true")

    args = p.parse_args()

    SHUB_USER = os.environ['SHUB_USER']
    SHUB_PASS = os.environ['SHUB_PASS']

    global logger
    logger = logging.getLogger('mapmaker')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s  %(name)s  %(levelname)s: %(message)s')

    file_handler = logging.FileHandler(args.log_file)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    if args.request_file is not None:
        global request
        request = json.loads(open(args.request_file).read())
    else:
        logger.info("Request file missing, aborting")
        raise Exception("Can't proceed without request json file")

    api_url = 'https://colhub.met.no/'
    api_url = 'https://scihub.copernicus.eu/dhus/'
    api = SentinelAPI(SHUB_USER, SHUB_PASS, api_url=api_url, show_progressbars=True)

    # Figure what the request coordinates should be
    # Replace ROI AIS part with actual coordinates
    if request['roi'] == "ais":
        ais_dict = get_ais_position()
        center_lon = ais_dict['lon']
        center_lat = ais_dict['lat']
    elif request["roi"] == "bbox":
        center_lat = request["center_lat"]
        center_lon = request["center_lon"]

    try:
        bbox_radius = request["box_radius"]
    except:
        bbox_radius = 1e5

    try:
        logger.info("Object position: {} {}".format(center_lat, center_lon))
        footprint = buffer_over_boat(center_lon, center_lat, buffer_radius=bbox_radius).wkt
        as_geojson = geojson.Feature(geometry=wkt.loads(footprint), properties={})
        request['roi'] = as_geojson['geometry']
    except:
        footprint = geojson_to_wkt(request['roi'])
        logger.debug("Search footprint is: {}".format(footprint))


    start_time = datetime.datetime.utcnow() - datetime.timedelta(hours=request['time_delta_hours'])
    end_time = datetime.datetime.utcnow()

    products = api.query(
        footprint,
        date = (start_time, end_time),
        platformname=request['platformname'],
        cloudcoverpercentage=(0, int(request['cloudcoverpercentage']))
    )

    n_scenes = len(products.keys())
    message = "Found {} scenes".format(n_scenes)
    logger.info(message)
    if n_scenes>0:
        logger.info("Scenes list:\n{}".format("\n".join(['\t'+products[key]['identifier'] for key in products.keys()])))
    else:
        logger.warn(" ".join(['Not enough scenes found',
                            'within last {} hours, aborting'.format(request["time_delta_hours"])]))
        sys.exit()

    output_path = args.output_file
    logger.debug("Output path: {}".format(output_path))
    if args.download_dir is None:
        data_dir = os.path.splitext(output_path)[0] + ".d"
    else:
        data_dir = args.download_dir

    logger.debug("Download data directory: {}".format(data_dir))

    if not os.path.exists(data_dir):
        os.mkdir(data_dir)

    logger.debug("Begin download")
    pool = multiprocessing.Pool(processes=1)
    output = pool.map(partial(download_sentinel_product,
                              products=products,
                              api=api,
                              output_path=data_dir,
                              request=request), products.keys())

    if args.download_only is True:
        logger.info("Asked to only download data, stopping")
        sys.exit()

    with tempfile.NamedTemporaryFile() as tfile:

        create_empty_dst(
            tfile.name,
            request['roi']['coordinates'],
            request['spatial_resolution'],
            request['crs'],
            rasterio.uint8
        )

        for product_key in products.keys():
            process_sentinel_scene(products[product_key], data_dir, tfile.name)

        subprocess.call([
            "gdal_translate",
            "-ot", "Byte",
            "-co", "COMPRESS=JPEG",
            "-co", "JPEG_QUALITY=70",
            "-scale",
            tfile.name,
            output_path
            ])

    if args.keep_temporary is False:
        logger.info('Removing temporary directory {}'.format(data_dir))
        remove_dir(data_dir)

if __name__ == "__main__":
    main()
